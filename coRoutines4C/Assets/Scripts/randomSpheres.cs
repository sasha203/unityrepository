﻿using UnityEngine;
using System.Collections;

public class randomSpheres : MonoBehaviour
{
    public GameObject spherePrefab;
    public int numbersOfSpheres = 6;
	// Use this for initialization
	void Start () {
        StartCoroutine(GenerateSpheres());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    Vector3 GenerateRandomPosition()
    {
        float randomX = Random.Range(-6.5f, 6.5f);
        float randomY = Random.Range(-3.5f, 3.5f);

        Vector3 randomPosition = new Vector3(randomX, randomY, 0); //Vecotr 3 is a data Type which stores the coorditaes of the Sphere
        return randomPosition;

    }

    IEnumerator GenerateSpheres()
    {
        for (int i = 0; i < numbersOfSpheres; i++)
        {
            
            if(i == 0)
            {
                //yield return = fejn se jerga jigi lura
                yield return new WaitForSeconds(2f); //waits 2 seconds before creating the first Sphere
                //yield is a keyword used in Coroutines to tell the compiler ther
                //it needs to return and continue execution at that point.
            }

            Instantiate(spherePrefab, GenerateRandomPosition(), Quaternion.identity);
            yield return new WaitForSeconds(3f);
        }
        //we are going to create a random sphere every 3 seconds.

    }
}
