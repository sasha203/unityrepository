﻿using UnityEngine;
using System.Collections;

public class cubeController: MonoBehaviour
{
    public float moveSpeed = 3f;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        float y = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;
        transform.Translate(x, y, 0);
        //move the cube depending on user controls.
	
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "sphereTag") ;
        {
            Destroy(col.gameObject);
        }
    }
}
