﻿using UnityEngine;
using System.Collections;

public class HandleClickScript : MonoBehaviour {
    // Renderers
    Renderer rTrue;
    Renderer rFalse;


    //Audio Variables Declaration
    public AudioClip flipFront;
    public AudioClip flipBack;
    public AudioClip ScoreSound;
    public AudioClip victoryTrack;

    

    // Use this for initialization
    void Start () {
          StartCoroutine(showCards()); // calling the method showCards on start-up
          
    }
	
	// Update is called once per frame
	void Update () {

        if(MainScript.counter == 18) 
        {
            MainScript.counter = 0;
            MainScript.timerOn = false;  // the timer will stop when all cards are matched.
            MainScript.showLbl = true;
            GetComponent<AudioSource>().PlayOneShot(victoryTrack);
            MainScript.scoreGreen = true; // Will trigger the if condition found in the OnGUI method in Mainscript.
            MainScript.timeGreen = true; // Will trigger another if condition found in the OnGUI method in Mainscript.
            //Debug.Log("test"); //testing
        }

    }

    IEnumerator showCards()//Show cards in the begining before hiding them    (Coroutine)
    {
        rTrue = GetComponent<Renderer>();
        rTrue.enabled = true;
        yield return new WaitForSeconds(2); // wait for 2 seconds
        rFalse = GetComponent<Renderer>();
        rFalse.enabled = false;  
    }

    IEnumerator hideCards() //Hide cards when they don't match (Coroutine)
    {
        GameObject[] firstCards = GameObject.FindGameObjectsWithTag(MainScript.tag);
        yield return new WaitForSeconds(0.2f); // wait for 0.2 milliseconds.

        foreach (GameObject card in firstCards) //Will loop each card in firstCards.
        {
            card.GetComponent<SpriteRenderer>().enabled = false;
        }
        gameObject.GetComponent<SpriteRenderer>().enabled = false; // the current card (second card)
    }


    void OnMouseDown()//The OnMouseDown Method
    {

       
            if (MainScript.firstClick == false)//Code related to the first click
            {
                gameObject.GetComponent<SpriteRenderer>().enabled = true;
                GetComponent<AudioSource>().PlayOneShot(flipFront); //Sound on when 1st card is clicked.
                MainScript.firstClick = true;
                MainScript.tag = gameObject.tag;
                MainScript.timerOn = true; //initialize timer on first click.

                gameObject.GetComponent<Collider2D>().enabled = false;// firstClick game object turned to false.
                MainScript.firstObject = gameObject; // set firstObject to gameObject
                
            }

            else if (MainScript.secondClick == false)
            {
                gameObject.GetComponent<SpriteRenderer>().enabled = true;
                MainScript.secondClick = true;
                GetComponent<AudioSource>().PlayOneShot(flipFront); //Sound on when 2nd card is clicked.

                if (MainScript.tag == gameObject.tag)
                {
                    gameObject.GetComponent<Collider2D>().enabled = false; // darba biss ghadu jitla lis score ma ghadekx tista terbah min fuq karta 1.
                    //Destroy(gameObject); //pruvajt id destroy imma qed tahdem sew

                    MainScript.score = MainScript.score += 2;
                    MainScript.counter++;
                    GetComponent<AudioSource>().PlayOneShot(ScoreSound); //sound will trigger when score will increase.
                    MainScript.firstClick = false;
                    MainScript.secondClick = false;
                    //Debug.Log("Matched");   //testing
                }

                else if (MainScript.tag != gameObject.tag)
                {
                    // Debug.Log("Not Matched");   //testing
                    MainScript.firstClick = false;
                    MainScript.secondClick = false;
                    StartCoroutine(hideCards()); //Calls hideCards to hide cards when they don't match.
                    GetComponent<AudioSource>().PlayOneShot(flipBack); // When two cards don't match the flip back audio will play.
                    MainScript.firstObject.GetComponent<Collider2D>().enabled = true;  // firstClick game object turned back to true

                }

            }
       







        }
    }



