﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainScript : MonoBehaviour {
    //PrefabList declaration
    List<GameObject> prefabList = new List<GameObject>();


    //Cards initial locaton.
    float x = -57.4f; // x initial position
    float y = 55f; // y initial position


    // Variable initialization
    public static bool firstClick = false;
    public static bool secondClick = false;
    public static int score = 0; //User's Score. 
    public static float timer = 0f;  //Game timer.
    public static bool timerOn = false; //timer bool
    public static int counter = 0; //Counter
    public static bool showLbl = false; //shows the label when true.

    //Green labels variables.
    public static bool scoreGreen = false; 
    public static bool timeGreen = false; 
    
    //tag declaration
    public static string tag;

    //GUISkins labels Declaration
    public GUISkin scoreLblSkin;
    public GUISkin timerLblSkin;
    public GUISkin victoryLblSkin;

    //GUISkins labels which will turn green.
    public GUISkin scoreGreenLbl;
    public GUISkin timeGreenLbl;

    public static GameObject firstObject; //variable for firstObject



    void Start () {
        prefabList.AddRange(Resources.LoadAll<GameObject>("Prefabs")); //call them twice so that we have 36 cards.  prefabs(18cards).
        prefabList.AddRange(Resources.LoadAll<GameObject>("Prefabs"));
        //Debug.Log(prefabList.Count);  //testing

        for (int i = 0; i < 36; i++)  //Number of Iterations
        {
            int rPosition = Random.Range(0, prefabList.Count); //will Generate a random number.
            GameObject cloneCard = Instantiate(prefabList[rPosition], new Vector3(x, y, 0), transform.rotation) as GameObject; // Will pick a prefab number according to the random number and will set the location as assigned.
            x += 20;
            prefabList.RemoveAt(rPosition);

            if (x == 62.6f)
            {
                y -= 20;  // y will decrement by 20.
                x = -57.4f; // x will be reseted to it's initial value.
            }
        }
    }
	    
	
	void Update () {
        if (timerOn) //if timeOn is true timer will initialize.
        {
            timer += Time.deltaTime;  //Timer incrementing.
        }
	
	}

    void OnGUI()
    {
        //score label
        GUI.skin = scoreLblSkin;
        GUI.Label(new Rect(Screen.width / 8, 30 , 200, 100), "Score: " + score);   //Screen.Width was used due to screen size varaity.

        if (scoreGreen == true)
        {
            GUI.skin = scoreGreenLbl;
            GUI.Label(new Rect(Screen.width / 8, 30 , 200, 100), "Score: " + score);
        }

        //Timer label
        GUI.skin = timerLblSkin;
        float mins = Mathf.Floor(timer / 60);
        float secs = timer % 60;
        GUI.Label(new Rect(Screen.width / 8, 60, 200, 100), "Min: " + mins + " Sec: " + Mathf.RoundToInt(secs));
        if (timeGreen == true)
        {
            GUI.skin = timeGreenLbl;
           
            // didn't enter the variables for mins & secs but worked fine.
            GUI.Label(new Rect(Screen.width / 8, 60, 200, 100), "Min: " + mins + " Sec: " + Mathf.RoundToInt(secs));
        }


        //Victory label
        if(showLbl == true)
        {
            GUI.skin = victoryLblSkin;
            GUI.Label(new Rect(Screen.width / 8, Screen.height / 2, 1000, 100), "All Cards Matched! You Did it in: " + "Min: " + mins + " Sec: " + Mathf.RoundToInt(secs) + " with the score of: " + score);
        }
       
        
    }
}
